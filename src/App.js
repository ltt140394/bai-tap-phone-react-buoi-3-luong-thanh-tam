import logo from './logo.svg';
import './App.css';
import PhoneStore from './PhoneStore/PhoneStore';

function App() {
  return (
    <div className="App">
      <PhoneStore />
    </div>
  );
}

export default App;
