import React, { Component } from 'react'


export default class Product extends Component {
    render() {
        let { tenSP, hinhAnh, giaBan, maSP } = this.props.dataProduct;

        return (
            <div className="col-4">
                <div className="card w-100 p-3" style={{ height: "495px", borderRadius: "5px" }}>
                    <img className="card-img-top" style={{ height: "320px" }} src={hinhAnh} alt="Card image cap" />
                    <div className="card-body p-0">
                        <h2 className="card-title">{tenSP}</h2>
                        <p class="card-text text-danger font-italic font-weight-bold">{new Intl.NumberFormat('vn-VN').format(giaBan)} VNĐ</p>
                        <button onClick={() => {
                            this.props.getProductInfo(maSP);
                        }} className="btn btn-success" data-toggle="modal" data-target="#exampleModal">Xem chi tiết</button>
                    </div>
                </div>
            </div>
        )
    }
}
