import React, { Component } from 'react'
import Product from './Product'

export default class ProductList extends Component {
    render() {
        return (
            <div>
                <div className="row">
                    {this.props.productList.map((item) => {
                        return <Product key={item.maSP} dataProduct={item} getProductInfo={this.props.getProductInfo} />
                    })}
                </div>
            </div>
        )
    }
}
