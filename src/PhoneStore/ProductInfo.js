import React, { Component } from 'react'

export default class ProductInfo extends Component {
    render() {
        let { maSP, tenSP, manHinh, heDieuHanh, cameraTruoc, cameraSau, ram, rom, giaBan, hinhAnh } = this.props.productInfo;

        return (
            <div className="modal fade w-100" id="exampleModal" tabIndex={-1} role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog" style={{ maxWidth: "900px" }} role="document">
                    <div className="modal-content">
                        <div className="modal-body pb-0">
                            <div className="row">
                                <div className="col-4 d-flex align-items-center">
                                    <div>
                                        <h2>{tenSP}</h2>
                                        <img src={hinhAnh} alt="" style={{ width: "100%" }} />
                                    </div>
                                </div>
                                <div className="col-8 px-4 pt-4">
                                    <h3 style={{ textAlign: "left" }}>Thông số kỹ thuật</h3>
                                    <table className="table w-100">
                                        <tbody>
                                            <tr className="row">
                                                <td className="col-4 text-left">Màn hình</td>
                                                <td className="col-8 text-left">{manHinh}</td>
                                            </tr>
                                            <tr className="row">
                                                <td className="col-4 text-left">Hệ điều hành</td>
                                                <td className="col-8 text-left">{heDieuHanh}</td>
                                            </tr>
                                            <tr className="row">
                                                <td className="col-4 text-left">Camera trước</td>
                                                <td className="col-8 text-left">{cameraTruoc}</td>
                                            </tr>
                                            <tr className="row">
                                                <td className="col-4 text-left">Camera sau</td>
                                                <td className="col-8 text-left">{cameraSau}</td>
                                            </tr>
                                            <tr className="row">
                                                <td className="col-4 text-left">RAM</td>
                                                <td className="col-8 text-left">{ram}</td>
                                            </tr>
                                            <tr className="row">
                                                <td className="col-4 text-left">ROM</td>
                                                <td className="col-8 text-left">{rom}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" data-dismiss="modal">Đóng</button>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}
