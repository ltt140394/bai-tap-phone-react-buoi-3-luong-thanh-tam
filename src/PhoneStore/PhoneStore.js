import React, { Component } from 'react'
import { phoneData } from "./phoneData.js"
import ProductList from './ProductList.js'
import styles from "./phoneStore.css"
import ProductInfo from './ProductInfo.js'

export default class PhoneStore extends Component {
    state = {
        productList: phoneData,
        productInfo: { ...phoneData[0] }
    }

    getProductInfo = (productID) => {
        let cloneProductList = [...phoneData];

        let index = cloneProductList.findIndex((item) => {
            return item.maSP == productID;
        });

        let currentProduct = cloneProductList[index];

        this.setState({
            productInfo: {
                tenSP: currentProduct.tenSP,
                manHinh: currentProduct.manHinh,
                heDieuHanh: currentProduct.heDieuHanh,
                cameraTruoc: currentProduct.cameraTruoc,
                cameraSau: currentProduct.cameraSau,
                ram: currentProduct.ram,
                rom: currentProduct.rom,
                hinhAnh: currentProduct.hinhAnh
            }
        });
    }

    render() {
        return (
            <div className="container py-3">
                <h1 className="text-success display-3">PHONE STORE</h1>
                <div className="my-5">
                    <ProductList
                        productList={this.state.productList}
                        getProductInfo={this.getProductInfo}
                    />
                </div>
                <ProductInfo productInfo={this.state.productInfo} />
            </div>
        )
    }
}
